package week3.Task1;

import week3.Task1.domain.Employee;
import week3.Task1.domain.Musician;
import week3.Task1.domain.Person;
import week3.Task1.domain.Programmer;

public class Main {
    public static void main(String[] args) {
        Person employee = new Employee("John", "Watson", 3000000);
        Person programmer = new Programmer("Steve", "Jobs", 100000000 , "Java");
        //Person -> Employee -> Programmer
        Person singer = new Musician("John", "Lennon", "guitar");

        //Casting -* when you convert one type to another
        //((Musician) singer).sing();

        System.out.println(employee);
        System.out.println(programmer);
    }
}
