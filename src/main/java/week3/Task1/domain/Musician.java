package week3.Task1.domain;

public class Musician extends Person{
    private String instrument;

    public Musician(String name, String surname, String instrument) {
        super(name, surname);
        setInstrument(instrument);
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public void sing() {
        System.out.println("Tralalala!");
    }

    @Override
    public String toString() {
        return super.toString() + " " + instrument;
    }
}
