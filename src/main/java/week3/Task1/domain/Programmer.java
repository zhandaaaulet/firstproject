package week3.Task1.domain;

public class Programmer extends Employee {
    private String language;

    public Programmer(String name, String surname, int salary, String language) {
        super(name, surname, salary);
        setLanguage(language);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return super.toString() + " " +language;
    }
}
