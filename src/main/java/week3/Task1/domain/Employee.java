package week3.Task1.domain;

public class Employee extends Person{
    private int salary;

    public Employee(String name, String surname, int salary) {
        super(name, surname);
        setSalary(salary);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + " " + salary;
    }
}

